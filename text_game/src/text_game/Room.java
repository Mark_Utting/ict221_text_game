package text_game;

/**
 * One room (or location) of the text-based adventure game.
 *
 * @author Mark Utting
 */
public class Room {
	private String description;
	private Room north;
	private Room east;
	private Room south;
	private Room west;
	
	public Room(String desc) {
		description = desc;
	}

	/**
	 * Connect the west exit of this room to the 'other' room.
	 * It also connects the EAST exit of that room to this room.
	 *
	 * @param other
	 */
	public void connectWest(Room other) {
		this.west = other;
		other.east = this;
	}

	/**
	 * Prints a description of this room and its contents.
	 */
	public void describe() {
		System.out.println(description);
	}
	
	/**
	 * Try to move in the given direction from this room.
	 *
	 * @param dir Should be "north", "east", "south" or "west".
	 * @return the new room.
	 */
	public Room move(String dir) {
		Room nextRoom = null;
		if (dir.equals("north")) {
			nextRoom = north;
		} else if (dir.equals("east")) {
			nextRoom = east;
		} else if (dir.equals("south")) {
			nextRoom = south;
		} else if (dir.equals("west")) {
			nextRoom = west;
		} else {
			System.out.println("Error: unknown direction " + dir);
		}
		if (nextRoom == null) {
			System.out.println("You cannot go " + dir + " from here.");
			nextRoom = this; // stay in this room
		} else {
			// TODO: check to see if a 'key' is required to enter nextRoom?
		}
		return nextRoom;		
	}
}
