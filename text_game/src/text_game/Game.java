package text_game;

import java.util.Scanner;

/**
 * A simple text-based adventure game.
 * 
 * @author Mark Utting
 */
public class Game {

	private Room hallway;
	private Room lounge;
	private Room bedroom;
	private Room exit;
	private Room current;
	
	public Game() {
		hallway = new Room("You are in a dark hallway.\n"
			+ "There is a table beside you, and a light to the west.\n");
		lounge = new Room("You are in a brightly-lit lounge, with two red sofas.\n"
			+ "A stereo is playing soft music.\n");
		bedroom = new Room("You are in a bedroom.\n"
			+ "You can see a bunk bed, two cricket bats, and an aquarium.\n");
		exit = new Room("You escaped outside.  Well done!\n");
		current = hallway;
		// Now connect the rooms together.
		exit.connectWest(hallway);
		hallway.connectWest(lounge);
		lounge.connectWest(bedroom);
	}
	
	/**
	 * Play the interactive text game.
	 * 
	 * Reads commands from the given input, line by line.
	 *
	 * @param in
	 */
	public void play(Scanner in) {
		current.describe();
		while (current != exit) {
			String line = in.nextLine().toLowerCase();
			if (line.equals("quit")) {
				System.out.println("You gave up!");
				break;
			} else if (line.startsWith("go ")) {
				current = current.move(line.substring(3));
				current.describe();
			} else if (line.startsWith("take ")) {
				// TODO: take(line.substring(5));
			} else {
				System.out.println("Unknown command '" + line + "'.  Try go/take/quit.");
			}
		}
	}

	/** Starts the whole game. */
	public static void main(String[] args) {
		Game game = new Game();
		game.play(new Scanner(System.in));
	}

}
